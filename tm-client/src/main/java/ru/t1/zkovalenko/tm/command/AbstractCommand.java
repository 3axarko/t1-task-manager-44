package ru.t1.zkovalenko.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.model.ICommand;
import ru.t1.zkovalenko.tm.api.service.IServiceLocator;
import ru.t1.zkovalenko.tm.enumerated.Role;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute();

    @NotNull
    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    public void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @Override
    public boolean isSimpleCommand() {
        return false;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final Integer spacesArgs = 7;
        @NotNull final Integer spacesNames = 40;
        @Nullable final String argument = getArgument();
        @NotNull final String name = getName();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (argument != null && !argument.isEmpty()) result += argument;
        result += spaceCounter(argument, spacesArgs);
        if (!name.isEmpty()) result += name;
        result += spaceCounter(name, spacesNames);
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    @NotNull
    private String spaceCounter(@Nullable String string, @NotNull final Integer maxSpace) {
        if (string == null) string = "";
        int resultCountSpaces = maxSpace - string.length();
        resultCountSpaces = resultCountSpaces > 0 ? resultCountSpaces : 1;
        @NotNull String resultSpaces = "";
        for (int i = 0; i < resultCountSpaces; i++) {
            resultSpaces += " ";
        }
        return resultSpaces;
    }

}
