package ru.t1.zkovalenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.model.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.model.ITaskRepository;
import ru.t1.zkovalenko.tm.api.repository.model.IUserRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.model.IUserService;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.exception.entity.ProjectNotFoundException;
import ru.t1.zkovalenko.tm.exception.field.IdEmptyException;
import ru.t1.zkovalenko.tm.exception.field.LoginEmptyException;
import ru.t1.zkovalenko.tm.exception.field.PasswordEmptyException;
import ru.t1.zkovalenko.tm.exception.field.RoleEmptyException;
import ru.t1.zkovalenko.tm.exception.user.EmailEmptyException;
import ru.t1.zkovalenko.tm.exception.user.ExistsEmailException;
import ru.t1.zkovalenko.tm.exception.user.ExistsLoginException;
import ru.t1.zkovalenko.tm.exception.user.UserNotFoundException;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.repository.model.ProjectRepository;
import ru.t1.zkovalenko.tm.repository.model.TaskRepository;
import ru.t1.zkovalenko.tm.repository.model.UserRepository;
import ru.t1.zkovalenko.tm.util.HashUtil;

import javax.persistence.EntityManager;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @NotNull
    public IUserRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        return add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        return add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull EntityManager entityManager = getEntityManager();
        @Nullable final User user;
        try {
            user = getRepository(entityManager).findByLogin(login);
        } finally {
            entityManager.close();
        }
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull EntityManager entityManager = getEntityManager();
        @Nullable final User user;
        try {
            user = getRepository(entityManager).findByEmail(email);
        } finally {
            entityManager.close();
        }
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User remove(@Nullable final User model) {
        if (model == null) throw new ProjectNotFoundException();
        final User user = super.remove(model);
        final String userId = user.getId();
        @NotNull EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final IUserRepository userRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear(userId);
            projectRepository.clear(userId);
            userRepository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findByLogin(login);
        return remove(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) {
        @NotNull final User user = findByEmail(email);
        return remove(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findByLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String id, @NotNull final String firstName,
            @NotNull final String lastName, @NotNull final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull EntityManager entityManager = getEntityManager();
        try {
            return getRepository(entityManager).findByLogin(login) != null;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new ExistsEmailException();
        @NotNull EntityManager entityManager = getEntityManager();
        try {
            return getRepository(entityManager).findByEmail(email) != null;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        user.setLocked(true);
        update(user);
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        user.setLocked(false);
        update(user);
    }

}
