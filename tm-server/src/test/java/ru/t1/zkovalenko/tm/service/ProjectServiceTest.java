package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.model.IProjectService;
import ru.t1.zkovalenko.tm.api.service.model.IUserService;
import ru.t1.zkovalenko.tm.marker.UnitServiceCategory;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.service.model.ProjectService;
import ru.t1.zkovalenko.tm.service.model.UserService;

import static ru.t1.zkovalenko.tm.constant.model.ProjectTestData.*;
import static ru.t1.zkovalenko.tm.constant.model.UserTestData.USER_LIST;
import static ru.t1.zkovalenko.tm.enumerated.Status.COMPLETED;
import static ru.t1.zkovalenko.tm.enumerated.Status.NOT_STARTED;

@Category(UnitServiceCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(
            connectionService,
            propertyService
    );

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Nullable
    private Project projectCreated;

    @Nullable
    private User userCreated;

    @Before
    public void before() {
        userCreated = userService.add(USER_LIST.get(0));
    }

    @After
    public void after() {
        if (projectCreated != null) projectService.remove(projectCreated);
        projectCreated = null;
        userService.remove(userCreated);
    }

    @Test
    public void changeProjectStatusById() {
        PROJECT1.setUser(userCreated);
        projectCreated = projectService.add(PROJECT1);
        Assert.assertEquals(projectCreated.getStatus(), NOT_STARTED);
        @NotNull final Project projectChanged = projectService
                .changeProjectStatusById(userCreated.getId(), projectCreated.getId(), COMPLETED);
        Assert.assertEquals(projectChanged.getStatus(), COMPLETED);
    }

    @Test
    public void changeProjectStatusByIndex() {
        PROJECT1.setUser(userCreated);
        projectCreated = projectService.add(PROJECT1);
        Assert.assertEquals(projectCreated.getStatus(), NOT_STARTED);
        @NotNull final Project projectChanged = projectService
                .changeProjectStatusByIndex(userCreated.getId(), 1, COMPLETED);
        Assert.assertEquals(projectChanged.getStatus(), COMPLETED);
    }

    @Test
    public void createUserIdName() {
        PROJECT1.setUser(userCreated);
        projectCreated = projectService.add(PROJECT1);
        @NotNull final Project projectFounded = projectService.findOneById(projectCreated.getId());
        Assert.assertEquals(PROJECT_NAME, projectFounded.getName());
    }

    @Test
    public void createUserIdNameDescription() {
        PROJECT1.setUser(userCreated);
        PROJECT1.setName(PROJECT_NAME);
        PROJECT1.setDescription(PROJECT_DESCRIPTION);
        projectCreated = projectService.add(PROJECT1);
        @NotNull final Project projectFounded = projectService.findOneById(projectCreated.getId());
        Assert.assertEquals(PROJECT_NAME, projectFounded.getName());
        Assert.assertEquals(PROJECT_DESCRIPTION, projectFounded.getDescription());
    }

    @Test
    public void updateById() {
        PROJECT1.setUser(userCreated);
        PROJECT1.setName(PROJECT_NAME);
        PROJECT1.setDescription(PROJECT_DESCRIPTION);
        projectCreated = projectService.add(PROJECT1);
        projectService.updateById(userCreated.getId(),
                projectCreated.getId(),
                PROJECT_NAME + "1",
                PROJECT_DESCRIPTION + "1");
        @NotNull final Project projectFounded = projectService.findOneById(projectCreated.getId());
        Assert.assertEquals(projectFounded.getName(), PROJECT_NAME + "1");
        Assert.assertEquals(projectFounded.getDescription(), PROJECT_DESCRIPTION + "1");
    }

    @Test
    public void updateByIndex() {
        PROJECT1.setUser(userCreated);
        PROJECT1.setName(PROJECT_NAME);
        PROJECT1.setDescription(PROJECT_DESCRIPTION);
        projectCreated = projectService.add(PROJECT1);
        @NotNull final Project projectFounded = projectService.findOneByIndex(1);
        @NotNull final Project projectChanged = projectService.updateByIndex(projectFounded.getUser().getId(),
                1,
                PROJECT_NAME + "1",
                PROJECT_DESCRIPTION + "1");
        Assert.assertEquals(projectChanged.getName(), PROJECT_NAME + "1");
        Assert.assertEquals(projectChanged.getDescription(), PROJECT_DESCRIPTION + "1");
    }

}
